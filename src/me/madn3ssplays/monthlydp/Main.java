package me.madn3ssplays.monthlydp;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import me.madn3ssplays.monthlydp.Commands.MonthlyDPCMD;

public class Main extends JavaPlugin {
	private static Main get;

	public void onEnable() {
		get = this;

		Bukkit.getPluginCommand("monthlydp").setExecutor(new MonthlyDPCMD());
	}

	public static Main getInstance() {
		return get;
	}
}
