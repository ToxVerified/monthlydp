package me.madn3ssplays.monthlydp.Commands;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class MonthlyDPCMD implements CommandExecutor {

	public Player randomPlayer() {
		return (Player) Bukkit.getOnlinePlayers().toArray()[new Random().nextInt(Bukkit.getOnlinePlayers().size())];
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg2, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c&l(!) &cOnly players may execute this command!"));
			return true;
		}
		Player p = (Player) sender;
		ItemStack i = p.getItemInHand();
		if (!p.hasPermission("monthlydp.use")) {
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c&l(!) &cYou don't have permission to execute this command!"));
			return true;
		}
		if (args.length == 0) {
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&b/MonthlyDp hand or handall|command <cmd>"));
			return true;
		}
		if (args.length == 1) {
			if (args[0].equalsIgnoreCase("hand")) {
				if (p.getItemInHand() == null || p.getItemInHand().getType() == Material.AIR) {
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c&l(!) &cYou may not give out air."));
					return true;
				}
				if (i.hasItemMeta() == true && i.getItemMeta().hasDisplayName() == true) {
					Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&bNext up &f: &c " + i.getItemMeta().getDisplayName() + "&c!"));
				} else {
					Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&bNext up &f: &c " + i.getType().toString().toLowerCase() + "!"));
				}
				randomPlayer().getInventory().addItem(i);
				randomPlayer().updateInventory();
				Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&bGoes to &f: &c " + randomPlayer().getName() + "!"));
				return true;
			}
			if (args[0].equalsIgnoreCase("handall")) {
				if (p.getItemInHand() == null || p.getItemInHand().getType() == Material.AIR) {
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c&l(!) &cYou may not give out air."));
					return true;
				}
				if (i.hasItemMeta() == true && i.getItemMeta().hasDisplayName() == true) {
					Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&bComing up &f: " + i.getItemMeta().getDisplayName() + "&c!"));
				} else {
					Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&bComing up &f: " + i.getType().toString().toLowerCase() + "!"));
				}
				Bukkit.getOnlinePlayers().forEach(all -> all.getInventory().addItem(i));
				Bukkit.getOnlinePlayers().forEach(all -> all.updateInventory());
				Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&bGoes to &f: &cAll online players!"));
				return true;
			}
		}
		if (args.length == 3) {
			if (args[0].equalsIgnoreCase("Command")) {
				String name = ChatColor.translateAlternateColorCodes('&', args[1]);
				String command = args[2].replace("_", " ").replace("#p", randomPlayer().getName());
				Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&bComing up &f: " + name + "!"));
				Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), command);
				Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&bGoes to &f: &c " + randomPlayer().getName() + "!"));
				return true;
			}
		}
		return true;
	}
}
